const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv/config')


const app = express();
app.use(cors())
app.use(bodyParser.json());

// Import Routs
const sellersRoute = require('./routes/sellers')
app.use('/sellers', sellersRoute);

const timeSlotsRout = require('./routes/timeSlots')
app.use('/timeSlots', timeSlotsRout);

// Connect to DB
mongoose.connect(
    process.env.DB_CONNECTION,
    {useUnifiedTopology: true},
    () => console.log("connected to MongoDB")
)


app.listen(3000);
