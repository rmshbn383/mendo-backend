const express = require('express');
const TimeSlot = require('../models/TimeSlot')
var ObjectId = require('mongoose').Types.ObjectId;

const router = express.Router();

// GET the TimeSlots
router.get('/', (req, res) => {
    const filter = req.query.filter;
    const findParam = !!filter ? {forSeller: ObjectId(filter)} : null;
    TimeSlot.find(findParam).sort({"day": 1})
        .then((data) => {
        res.status(200).json(data)
    }).catch(error => {
        res.status(500).json(error)
    })

})

// Submit a TimeSlot
router.post('/', (req, res) => {
    const timeSlot = new TimeSlot({
        day: req.body.day,
        hour: req.body.hour,
        isBooked: req.body.isBooked,
        forSeller: ObjectId(req.body.forSeller),
    })

    timeSlot.save()
        .then(data => {
            res.status(200).json(data);
        })
        .catch(error => {
            res.status(500).json({message: error});
        });
});

// DELETE One TimeSlot
router.patch('/:id', (req, res) => {
    TimeSlot.updateOne({_id: req.params.id}, {
        day: req.body.day,
        hour: req.body.hour,
        isBooked: req.body.isBooked,
        forSeller: ObjectId(req.body.forSeller),
    }).then((data) => {
        res.status(200).json(data)
    }).catch(error => {
        res.status(500).json(error)
    });
})

module.exports = router;
