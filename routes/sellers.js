const express = require('express');
const Seller = require('../models/Seller')

const router = express.Router();

// GET the Sellers
router.get('/', (req, res) => {
    const filter = req.query.filter;
    const findParam = !!filter ?
        {$or: [
                {name: {$regex: new RegExp(filter, "i")}},
                {service: {$regex: new RegExp(filter, "i")}}
            ]}
        : null;
    Seller.find(findParam)
        .then((data) => {
            res.status(200).json(data)
        }).catch(error => {
        res.status(500).json(error)
    })

})

// GET One Seller
router.get('/:id', (req, res) => {
    Seller.findById(req.params.id).then((data) => {
        res.status(200).json(data)
    }).catch(error => {
        res.status(500).json(error)
    })

})

// Submit a Seller
router.post('/', (req, res) => {
    const seller = new Seller({
        name: req.body.name,
        service: req.body.service
    });
    seller.save()
        .then(data => {
            res.status(200).json(data);
        })
        .catch(error => {
            res.status(500).json({message: error});
        });
});


// DELETE One Seller
router.delete('/:id', (req, res) => {
    Seller.remove({_id: req.params.id}).then((data) => {
        res.status(200).json({})
    }).catch(error => {
        res.status(500).json(error)
    });
})

module.exports = router;
