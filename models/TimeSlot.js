const mongoose = require('mongoose');

const TimeSlotSchema = mongoose.Schema({
    day: {
        type: String,
        require: true,
    },
    hour: {
        type: String,
        require: true
    },
    isBooked: {
        type: Boolean,
        require: true,
        default: false
    },
    forSeller: {type: mongoose.Schema.Types.ObjectId, ref: 'Seller'}
});

module.exports = mongoose.model('TimeSlot', TimeSlotSchema);
