const mongoose = require('mongoose');

const SellerSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    service: {
        type: String,
        require: true
    },
});

module.exports = mongoose.model('Sellers', SellerSchema);
